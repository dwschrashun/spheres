name: "Auriga",
stars: [
	{x:350 , y:40 , nextX:350, nextY:85 , note: "F#4"},
	{x:350 , y:85 , nextX: 350, nextY: 120, note: "C#4"},
	{x:350 , y:120 , nextX: 385, nextY: 150, note: "E4"},
	{x:385 , y:150 , nextX: 415, nextY: 135, note: "A3"},
	{x:415, y:135 , nextX: 410 , nextY: 100, note: "G#3"},
	{x:415 , y:90 , nextX: 350, nextY:40 , note: "C#3"},
],


name: "Perseus",
stars: [
	{x:800 , y:40 , nextX:725, nextY:70 , note: "F#4"},
	{x:725 , y:70 , nextX: 680, nextY: 110, note: "C#4"},
	{x:700 , y:90 , nextX: 725, nextY: 70, note: "E4"},
	{x:650 , y:80 , nextX: 700, nextY: 90, note: "A3"},
	{x:680, y:110 , nextX: 800 , nextY: 110, note: "G#3"},
	{x:700 , y:150 , nextX: 680, nextY: 100 , note: "C#3"},
	{x:800 , y:110 , nextX: 800, nextY: 90, note: "E4"},
	{x:875 , y:120 , nextX: 800, nextY: 110, note: "A3"},
	{x:800, y:90 , nextX: 800 , nextY: 40, note: "G#3"},
	{x:950 , y:80 , nextX: 950, nextY:90 , note: "C#3"},
],


// name: "Lynx",
// stars: [
// 	{x:300 , y:120 , nextX:320, nextY:100 , note: "A3"},
// 	{x:320 , y:100 , nextX: 380, nextY: 70, note: "C#3"},
// 	{x:380 , y:50 , nextX: 440, nextY: 40, note: "E4"},
// 	{x:440 , y:50 , nextX: 500, nextY: 15, note: "B3"},
// 	{x:500 , y:50 , nextX: 575 , nextY: 10, note: "G#3"},
// 	{x:575 , y:10 , nextX: 575, nextY:10 , note: "C#4"},
// ],



		// name: "square",
		// stars: [
		// 	{x: 200, y: 200, nextX: 300, nextY: 200, note: "B4"},
		// 	{x: 300, y: 200, nextX: 300, nextY: 300, note: "D3"},
		// 	{x: 300, y: 300, nextX: 200, nextY: 300, note: "E3"},
		// 	{x: 200, y: 300, nextX: 200, nextY: 200, note: "F#3"}
		// ]},
